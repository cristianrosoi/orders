const ordersData = require("./data/data.json");
const weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

class Orders {

  averageOrder(ordersData, weekday) {
    const orderLines = [];
    const ordersPerWeekday = ordersData.filter(
      (order) => new Date(order.creationDate).getDay() === weekday
    );

    for (let order of ordersPerWeekday) {
      orderLines.push(this.getAverageOrderLines(order.orderLines));
    }

    const result = {
      total: this.countByKey(orderLines, "total"),
      count: this.countByKey(orderLines, "count"),
      average: 0,
    };

    result.average = this.getAverage(result.count, result.total);

    console.log(result, weekdays[weekday]);
    return result;
  }

  getAverage(count, total) {
    if (count >= 0 && total >= 0) {
      let average = total / count;
      return isNaN(average) ? 0 : Math.round(average * 100) / 100;
    } else {
      return 0;
    }
  }

  countByKey(list, key) {
    try {
      return list.map((item) => item[key]).reduce((a, b) => a + b);
    } catch (e) {
      return 0;
    }
  }

  getAverageOrderLines(orderLines) {
    const result = {
      total: orderLines.map((order) => order.quantity).reduce((a, b) => a + b),
      count: orderLines.length,
      average: 0,
    };

    result.average = this.getAverage(result.count, result.total);

    return result;
  }
}

// check script
(function main() {
  for (let i = 0; i < 7; i++) {
    const orders = new Orders();
    orders.averageOrder(ordersData, i);
  }
})();
